use std::process;
use ipcheck::*;
use clap::Parser;

fn main() {
    ipcheck::print_banner();
    let config = Config::parse();
    if let Err(e) = ipcheck::run(config){
        println!("[!] Application error: {e}");
        process::exit(1);
    }
}
