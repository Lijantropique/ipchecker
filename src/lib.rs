use clap::Parser;
use std::{fs, io, collections, path};
use regex::Regex;
use whois_rust::{WhoIs, WhoIsLookupOptions};

// ***** General I/O functions *****
pub fn print_banner(){
    println!("-----------------------
(rust-ic 🦀) IP Checker 
Version 1.0 - Dec 2022
-----------------------\n");
}

pub fn print_help(){
    println!("IP checker - V1.0
Usage: ipcheck [OPTIONS] <ip_list.txt>
    OPTIONS (if ommited will return all):
    - a: return all information
    - c: return only country

    <ip_list.txt>: a text file with IPs to analyze, one per line\n");
}

// ***** WhoIs related functions *****
#[derive(Parser)]
#[clap(author="lijantropique", version="1.0", about="A Very simple whois IP checker. Just provide a txt file with IPs (one per line)")]
pub struct Config{
    /// Only extract country
    #[arg(short = 's', long = "short", action)]
    pub short: bool,

    /// IP list to be analyzed
    // #[arg(short = 'f', long = "file")]
    #[arg(value_name = "FILE")]
    pub file_path: path::PathBuf,
}

pub fn run(config:Config) -> Result<(), io::Error>{
    println!("[+] Starting IP analysis from '{}' ...", config.file_path.display());
    // let content = fs::read_to_string(config.file_path)?;
    let content = match fs::read_to_string(config.file_path) {
        Ok(file) => file,
        Err(_) => return Err(io::Error::new(io::ErrorKind::Other, "<FILE> with IPs is not available.")),
    };
    // let servers = fs::read_to_string("./servers.json")?;
    let servers = match fs::read_to_string("servers.json") {
        Ok(file) => file,
        Err(_) => return Err(io::Error::new(io::ErrorKind::Other, "servers.json (config file) not available.")),
    };
    // let whois =  WhoIs::from_path("./server.json").unwrap();
    let whois =  WhoIs::from_string(servers).unwrap();
    let mut wtr = csv::Writer::from_path("foo.csv")?;

    // write csv headers
    wtr.write_record(&["IP", "NetRange", "Organization", "Country"])?;

    for line in content.lines(){
        let mut tmp = Vec::new();
        tmp.push(line.to_string());
        println!("\t[-] Processing {} ...", line);
        tmp.append(&mut search_ip(line, &whois));
        wtr.write_record(&tmp)?;
    }

    println!("[+] Analysis completed. Check output.csv for results\n");
    wtr.flush()?;
    Ok(())
}
pub fn search_ip(ip:&str, whois_instance:&WhoIs)-> Vec<String>{
    
    let mut v = Vec::new();
    let result: String = whois_instance.lookup(WhoIsLookupOptions::from_string(ip).unwrap()).unwrap();
    // pass the regest to a separate string file so it is easier to update
    // finally check this https://docs.rs/regex/latest/regex/#example-avoid-compiling-the-same-regex-in-a-loop

    let queries = collections::HashMap::from([
        ("Netrange", r"NetRange:\s*(.*)|inetnum:\s*(.*)"),
        ("Organization", r"Organization:\s*(.*)|org-name:\s*(.*)"),
        ("Country", r"Country:\s*(.*)|country:\s*(.*)"),
    ]);
    for (key,value) in queries {
        v.push(parse_regex(&result, value, key));
    }
    v
    // println!("{result}");
}

fn parse_results(caps:regex::Captures)->String{
    let n = caps.len();
    let mut tmp  = "";
    for idx in 1..n{
        if tmp == "" {
            tmp = caps.get(idx).map_or("", |m| m.as_str());
        }
    }
    format!("{}", tmp)
}

fn parse_regex(result:&str, query:&str, keyword:&str)-> String{
    let re = Regex::new(query).unwrap();
    let caps  = re.captures(&result).unwrap();
    let tmp = parse_results(caps); 
    println!("\t\t{}: {}", keyword, tmp);
    tmp
}